#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>
#define PORT 8080

void removeSpaces(char *str)
{
    // To keep track of non-space character count
    int count = 0;
 
    // Traverse the given string. If current character
    // is not space, then place it at index 'count++'
    for (int i = 0; str[i]; i++)
        if (str[i] != ' ')
            str[count++] = str[i]; // here count is
                                   // incremented
    str[count] = '\0';
}

int writeLog(char* command){
        time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char s[64];
    assert(strftime(s, sizeof(s), "%F %X", tm));

    char user[100];
    cuserid(user);

    FILE* fp = fopen("../log.txt", "a");

    if(fp == NULL){
        return -1;
    }

    fprintf(fp, "%s:%s:%s", s, user, command);
    fclose(fp);
}

int getDBName(char* command){
    char dbname[100] = "";
    int i, flag = 0, idx = 0;
    for(i = 0; i < strlen(command); i++){
        if(command[i] == ' ' || command[i] == ';'){
            flag++;
        }

        if(flag == 2){
            dbname[idx] = '\0';
            break;
        }

        if(flag == 1 && command[i] != ';' && command[i] != ' '){
            dbname[idx++] = command[i];
        }
    }

    strcpy(command, dbname);

    return 0;
}

int getTableName(char* command){
    char tablename[100] = "";
    int i, flag = 0, idx = 0;
    for(i = 0; i < strlen(command); i++){
        if(command[i] == ' ' || command[i] == ';'){
            flag++;
            continue;
        }

        if(flag == 2 && command[i] != ';'){
            tablename[idx++] = command[i];
        }

        if(flag == 3){
            tablename[idx] = '\0';
            break;
        }
    }
    strcpy(command, tablename);

    return 0;
}

int getColumns(char* commmand){
    char columns[100] = "";

    int i, flag = 0, idx = 0;
    for(i = 0; i < strlen(commmand); i++){
        if(commmand[i] == '('){
            flag = 1;
        }

        if(flag == 1 && commmand[i] != ',' && commmand[i] != ')' && commmand[i] != '('){
            columns[idx++] = commmand[i];
        }else if(flag == 1 && commmand[i] == ','){
            continue;
        }else if(flag == 1 && commmand[i] == ')'){
            break;
        }
    }

    strcpy(commmand, columns);

    return 0;
}

int createTable(char* command, char* dbname){
    printf("creating table\n");

    printf("command: %s\n", command);
    char columns[100] = "";
    char newtable[100] = "";
    strcpy(columns, command);
    strcpy(newtable, command);
    getColumns(columns);
    getTableName(newtable);

    char fpath[500] = "";
    sprintf(fpath, "databaseku/%s/%s.txt", dbname, newtable);

    printf("%s\n", fpath);

    FILE* fp = fopen(fpath, "a");

    fprintf(fp, "%s\n", columns);

    fclose(fp);
}

int createDB(char* command){
    printf("creating database\n");
    char* newdb = strrchr(command, ' ');
    removeSpaces(newdb);

    char fpath[100] = "databaseku/";
    strcat(fpath, newdb);

    char cmd[200];
    sprintf(cmd, "mkdir -p %s", fpath);
    system(cmd);
}

int dropDB(char* command){
    printf("dropping database\n");
    char* newdb = strrchr(command, ' ');
    removeSpaces(newdb);

    char fpath[100] = "databaseku/";
    strcat(fpath, newdb);
    char cmd1[250];
    sprintf(cmd1, "rm -rf %s", fpath);
    system(cmd1);
}

int dropTable(char* command, char* dbname){
    char columns[100] = "";
    char newtable[100] = "";
    strcpy(newtable, command);
    getTableName(newtable);

    printf("%s ayam goreng", newtable);

    char fpath[200] = "";
    sprintf(fpath, "databaseku/%s/%skmasd", dbname, newtable);
    printf("%s", fpath);
    remove(fpath);
}

int dropColumn(char* command){
    printf("dropping column\n");
}

int insert(char* command){
    printf("inserting\n");
}

int update(char* command){
    printf("updating\n");
}

int delete(char* command){
    printf("deleting\n");
}

int selection(char* command){
    printf("selecting\n");
}

bool validateCommand(char* command){
    return strstr(command, "CREATE DATABASE") != NULL || strstr(command, "CREATE TABLE") != NULL || 
            strstr(command, "DROP DATABASE") != NULL || strstr(command, "DROP TABLE") != NULL || strstr(command, "DROP COLUMN") != NULL ||
            strstr(command, "INSERT INTO") != NULL || strstr(command, "UPDATE") != NULL || strstr(command, "DELETE FROM") != NULL || 
            strstr(command, "SELECT") != NULL || strstr(command, "USE") != NULL;
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    while(1){
        char buffer[1024] = {0};
        valread = read(new_socket , buffer, 1024);
        if(strcmp(buffer, "STOP") == 0){
            break;
        }
        
        if(validateCommand(buffer)){
            if(strstr(buffer, "CREATE DATABASE") != NULL){
                createDB(buffer);
            }else if(strstr(buffer, "CREATE TABLE") != NULL){
                printf("Please use the database first before creating new table\n");
            }else if(strstr(buffer, "DROP DATABASE") != NULL){
                dropDB(buffer);
            }else if(strstr(buffer, "DROP TABLE") != NULL){
                printf("Please use the database first before deleting table\n");
            }else if(strstr(buffer, "DROP COLUMN") != NULL){
                dropColumn(buffer);
            }else if(strstr(buffer, "INSERT INTO") != NULL){
                insert(buffer);
            }else if(strstr(buffer, "UPDATE") != NULL){
                update(buffer);
            }else if(strstr(buffer, "DELETE FROM") != NULL){
                delete(buffer);
            }else if(strstr(buffer, "SELECT") != NULL){
                selection(buffer);   
            }else if(strstr(buffer, "USE") != NULL){
                char* dbname = buffer;
                getDBName(dbname);

                char buffer[1024] = {0};
                valread = read(new_socket , buffer, 1024);
    
                if(strstr(buffer, "CREATE TABLE") != NULL){
                    createTable(buffer, dbname);
                }else if(strstr(buffer, "DROP TABLE") != NULL){
                    dropTable(buffer, dbname);
                }
            }

            writeLog(buffer);
        }else{
            printf("No command provided.");
        }

    }

    return 0;
}